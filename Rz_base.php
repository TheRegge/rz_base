<?php
/**
* Base class for preventing boilerplate
*/
class Rz_base
{
	/**
	 * Array to hold class params;
	 * @var array
	 */	
	protected $data = array();

	/**
	 * Array to store errors
	 * @var array
	 */
	protected $errors = array();

	public function __construct()
	{
		# code...
	}

	/**
     * Magic function setting data
     * @param  string $name data key name
     * @return mixed       null or the data requested
     */
    public function __get( $name ) {

        if ( array_key_exists($name, $this->data) )
        {
            return $this->data[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    /**
     * Magic function getting data
     * @param string $name  data key name
     * @param mixed $value the value to set the data to.
     */
    public function __set( $name, $value ) {
        $this->data[$name] = $value;
    }

    /**
     * As of PHP 5.1.0
     * @param  string  $name The name of the data to check
     * @return boolean
     */
    public function __isset( $name )
    {
        return isset( $this->data[$name] );
    }

    /**
     * As of PHP 5.1.0
     * @param string $name the name of the data to unset
     */
    public function __unset( $name )
    {
        unset( $this->data[$name] );
    }
}